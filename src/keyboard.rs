use std::{sync::mpsc, thread, time::Duration};

#[derive(Debug)]
pub enum InputEvent<I> {
    Input(I),
    Tick,
}

pub struct Inputs {
    rx: std::sync::mpsc::Receiver<InputEvent<tui_textarea::Input>>,
    _tx: std::sync::mpsc::Sender<InputEvent<tui_textarea::Input>>,
}

impl Inputs {
    pub fn new(tick_rate: Duration) -> Inputs {
        let (tx, rx) = mpsc::channel();

        let event_tx = tx.clone();
        thread::spawn(move || {
            loop {
                // poll for tick rate duration, if no event, sent tick event.
                if crossterm::event::poll(tick_rate).unwrap() {
                    let input = crossterm::event::read().unwrap().into();
                    event_tx.send(InputEvent::Input(input)).unwrap();
                }

                event_tx.send(InputEvent::Tick).unwrap();
            }
        });

        Inputs { rx, _tx: tx }
    }

    /// Attempts to read an event.
    pub fn next(&mut self) -> Result<InputEvent<tui_textarea::Input>, mpsc::RecvError> {
        self.rx.recv()
    }
}

use crate::{App, IoEvent};

use std::sync::Arc;

pub struct IoAsyncHandler<'b> {
    app: Arc<tokio::sync::Mutex<App<'b>>>,
}

impl<'b> IoAsyncHandler<'b> {
    pub fn new(app: Arc<tokio::sync::Mutex<App<'b>>>) -> Self {
        Self { app }
    }

    /// We could be async here
    pub async fn handle_io_event(&mut self, io_event: IoEvent) {}
}

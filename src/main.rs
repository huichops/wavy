mod events;
mod keyboard;

mod prelude {
    pub use anyhow::Result;

    pub use std::{error::Error, io};
    pub use tui::{
        backend::{Backend, CrosstermBackend},
        layout::{Alignment, Constraint, Direction, Layout, Rect},
        style::{Color, Style},
        text::{Span, Spans},
        widgets::{Block, Borders, Paragraph},
        Frame, Terminal,
    };

    pub use crossterm::{
        event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
        execute,
        terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    };

    pub use tokio::sync::mpsc;
    pub use tokio::sync::mpsc::{Receiver, Sender};
    pub use tokio::sync::Mutex;
    pub use tui_textarea::{Input, Key, TextArea};

    #[derive(Debug)]
    pub enum IoEvent {
        Fetch { value: String },
    }

    #[derive(Debug)]
    pub enum RequestState {
        Idle,
        Loading,
        Done,
    }

    pub struct App<'a> {
        pub result: String,
        pub is_loading: bool,
        pub state: RequestState,
        pub address_bar: TextArea<'a>,
        pub sender: Sender<IoEvent>,
    }
}

use events::IoAsyncHandler;
use keyboard::{InputEvent, Inputs};
use prelude::*;
use std::sync::Arc;
use std::time::Duration;

enum AppReturn {
    Exit,
    Continue,
}

impl<'a> App<'a> {
    fn new(sender: Sender<IoEvent>) -> App<'a> {
        App {
            sender,
            address_bar: TextArea::from(vec!["https://httpbin.org/ip"]),
            result: String::from(""),
            state: RequestState::Idle,
            is_loading: false,
        }
    }

    pub async fn dispatch(&mut self, action: IoEvent) {
        // `is_loading` will be set to false again after the async action has finished in io/handler.rs
        self.is_loading = true;
        println!("dos {action:?}");
        if let Err(err) = self.sender.send(action).await {
            self.is_loading = false;
            println!("Error: {err:?}");
        };
    }

    pub fn set_response(&mut self, response: String) {
        self.result = response;
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    // setup terminal

    let (tx, mut mx) = mpsc::channel::<IoEvent>(32);
    // create app and run it
    //
    let app = Arc::new(tokio::sync::Mutex::new(App::new(tx.clone())));
    let app_ui = Arc::clone(&app);

    tokio::spawn(async move {
        let mut handler = IoAsyncHandler::new(app);
        while let Some(io_event) = mx.recv().await {
            println!("tres");
            handler.handle_io_event(io_event).await;
        }
    });

    run_app(&app_ui).await?;

    Ok(())
}

async fn run_app(app: &Arc<Mutex<App<'_>>>) -> Result<()> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let tick_rate = Duration::from_millis(200);
    let mut inputs = Inputs::new(tick_rate);

    loop {
        let mut app = app.lock().await;
        app.address_bar.set_block(
            Block::default()
                .borders(Borders::ALL)
                .title("Crossterm Minimal Example"),
        );

        terminal.draw(|f| ui(f, &app))?;
        match inputs.next()? {
            InputEvent::Input(input) => {
                if let Input { key: Key::Esc, .. } = input {
                    break;
                }

                if let Key::Enter = input.key {
                    app.state = RequestState::Loading;
                    let resp = reqwest::get(app.address_bar.lines()[0].clone())
                        .await
                        .unwrap()
                        .text()
                        .await
                        .unwrap();

                    app.state = RequestState::Done;
                    app.result = resp;
                }
                // println!("{:?}", input.key);
            }
            InputEvent::Tick => (),
        };
    }

    // restore terminal
    terminal.show_cursor()?;

    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;

    Ok(())
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &App) {
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
        .split(f.size());

    let block = Block::default().title("Collection").borders(Borders::ALL);
    f.render_widget(block, chunks[0]);

    let execution_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
        .split(chunks[1]);

    f.render_widget(app.address_bar.widget(), execution_chunks[0]);

    let result_text = match app.state {
        RequestState::Idle => Spans::from("Press 'Enter' to fetch data."),
        RequestState::Loading => Spans::from("Loading..."),
        RequestState::Done => Spans::from(format!("Response: \n{:?}", app.result)),
    };

    let block = Block::default().title("Result").borders(Borders::ALL);
    let paragraph = Paragraph::new(result_text)
        .style(Style::default().bg(Color::White).fg(Color::Black))
        .block(block)
        .alignment(Alignment::Left);

    f.render_widget(paragraph, execution_chunks[1]);
}
